package io.makeabilitylab.facetrackerble;

/**
 * FacePosition stores the current face x coordinates and applies a smoothing average.
 **/

public class FacePosition {

    final static int SMOOTHING_WINDOW_SIZE = 10;
    private float _posValueHistory[] = new float[SMOOTHING_WINDOW_SIZE];
    private int mCurrentAvg;

    public int currentPos(boolean isFrontFacing) {
        if(isFrontFacing){
            return 255 - mCurrentAvg;
        }
        return 255 - mCurrentAvg;
    }

    public void update(float xPos){
        if(xPos < 0){
            xPos = 0;
        }
        else if(xPos > 255){
            xPos = 255;
        }

        updateHistory(xPos);
        updateAvg();
    }

    private void updateHistory(float xPos){
        // updates our historical values used to smooth the velocity
        float next = xPos;
        for(int i = 0; i < SMOOTHING_WINDOW_SIZE; i++){
            float tmp = _posValueHistory[i];
            _posValueHistory[i] = next;
            next = tmp;
        }
    }

    private void updateAvg(){
        // find the average from our historical values
        float total = 0;
        for(int i = 0; i < SMOOTHING_WINDOW_SIZE; i++){
            total += _posValueHistory[i];
        }

        mCurrentAvg = Math.round(total / (float)SMOOTHING_WINDOW_SIZE);
    }
}

package io.makeabilitylab.facetrackerble;

import android.util.Log;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.Landmark;
import static com.google.android.gms.vision.face.Landmark.*;

/**
 * Stores facial landmark information and a comparison function
 * for determining if a new face matches the stored face.
 **/
public class FaceStore {
    private static final String TAG = "FaceStore";
    private float THRESHOLD = 0.4f;

    // face landmark vars
    private float mBottomMouthX;
    private float mLeftCheekX;
    private float mLeftEarX;
    private float mLeftEarTipX;
    private float mLeftEyeX;
    private float mLeftMouthX;
    private float mNoseBaseX;
    private float mRightCheekX;
    private float mRightEarX;
    private float mRightEarTipX;
    private float mRightEyeX;
    private float mRightMouthX;
    private float mBottomMouthY;
    private float mLeftCheekY;
    private float mLeftEarY;
    private float mLeftEarTipY;
    private float mLeftEyeY;
    private float mLeftMouthY;
    private float mNoseBaseY;
    private float mRightCheekY;
    private float mRightEarY;
    private float mRightEarTipY;
    private float mRightEyeY;
    private float mRightMouthY;
    private float mHWRatio;
    private double mEyeDist;
    private double mNoseDist;
    private double mMouthWidth;
    private double mLeftEyeMouthDist;
    private double mRightEyeMouthDist;

    public void buildThreshold(Face mFace) {
        this.THRESHOLD = comparisonVal(mFace) + 0.2f;
    }

    public float comparisonVal(Face face){
        float totalDiff = 0.0f;
        float centerOfFaceX = face.getPosition().x + (face.getWidth() / 2.0f);
        float centerOfFaceY = face.getPosition().y + (face.getHeight() / 2.0f);
        float xScale = face.getWidth();
        float yScale = face.getHeight();
        float thisBottomMouthX = 0;
        float thisLeftCheekX = 0;
        float thisLeftEarX = 0;
        float thisLeftEarTipX = 0;
        float thisLeftEyeX = 0;
        float thisLeftMouthX = 0;
        float thisNoseBaseX = 0;
        float thisRightCheekX = 0;
        float thisRightEarX = 0;
        float thisRightEarTipX = 0;
        float thisRightEyeX = 0;
        float thisRightMouthX = 0;
        float thisBottomMouthY = 0;
        float thisLeftCheekY = 0;
        float thisLeftEarY = 0;
        float thisLeftEarTipY = 0;
        float thisLeftEyeY = 0;
        float thisLeftMouthY = 0;
        float thisNoseBaseY = 0;
        float thisRightCheekY = 0;
        float thisRightEarY = 0;
        float thisRightEarTipY = 0;
        float thisRightEyeY = 0;
        float thisRightMouthY = 0;
        for(Landmark l : face.getLandmarks()){
            switch(l.getType()){
                case BOTTOM_MOUTH:
                    thisBottomMouthX = Math.abs((l.getPosition().x - centerOfFaceX)/xScale);
                    thisBottomMouthY = Math.abs((l.getPosition().y - centerOfFaceY)/yScale);
                case LEFT_CHEEK:
                    thisLeftCheekX = Math.abs((l.getPosition().x - centerOfFaceX)/xScale);
                    thisLeftCheekY = Math.abs((l.getPosition().y - centerOfFaceY)/yScale);
                case LEFT_EAR:
                    thisLeftEarX = Math.abs((l.getPosition().x - centerOfFaceX)/xScale);
                    thisLeftEarY = Math.abs((l.getPosition().y - centerOfFaceY)/yScale);
                case LEFT_EAR_TIP:
                    thisLeftEarTipX = Math.abs((l.getPosition().x - centerOfFaceX)/xScale);
                    thisLeftEarTipY = Math.abs((l.getPosition().y - centerOfFaceY)/yScale);
                case LEFT_EYE:
                    thisLeftEyeX = Math.abs((l.getPosition().x - centerOfFaceX)/xScale);
                    thisLeftEyeY = Math.abs((l.getPosition().y - centerOfFaceY)/yScale);
                case LEFT_MOUTH:
                    thisLeftMouthX = Math.abs((l.getPosition().x - centerOfFaceX)/xScale);
                    thisLeftMouthY = Math.abs((l.getPosition().y - centerOfFaceY)/yScale);
                case NOSE_BASE:
                    thisNoseBaseX = Math.abs((l.getPosition().x - centerOfFaceX)/xScale);
                    thisNoseBaseY = Math.abs((l.getPosition().y - centerOfFaceY)/yScale);
                case RIGHT_CHEEK:
                    thisRightCheekX = Math.abs((l.getPosition().x - centerOfFaceX)/xScale);
                    thisRightCheekY = Math.abs((l.getPosition().y - centerOfFaceY)/yScale);
                case RIGHT_EAR:
                    thisRightEarX = Math.abs((l.getPosition().x - centerOfFaceX)/xScale);
                    thisRightEarY = Math.abs((l.getPosition().y - centerOfFaceY)/yScale);
                case RIGHT_EAR_TIP:
                    thisRightEarTipX = Math.abs((l.getPosition().x - centerOfFaceX)/xScale);
                    thisRightEarTipY = Math.abs((l.getPosition().y - centerOfFaceY)/yScale);
                case RIGHT_EYE:
                    thisRightEyeX = Math.abs((l.getPosition().x - centerOfFaceX)/xScale);
                    thisRightEyeY = Math.abs((l.getPosition().y - centerOfFaceY)/yScale);
                case RIGHT_MOUTH:
                    thisRightMouthX = Math.abs((l.getPosition().x - centerOfFaceX)/xScale);
                    thisRightMouthY = Math.abs((l.getPosition().y - centerOfFaceY)/yScale);
            }
        }

        // add up the diffs
        totalDiff += Math.abs(mBottomMouthX - thisBottomMouthX);
        Log.d(TAG, "1 "+ totalDiff);
        totalDiff += Math.abs(mLeftCheekX - thisLeftCheekX);
        Log.d(TAG, "2 "+ totalDiff);
        totalDiff += Math.abs(mLeftEarX - thisLeftEarX);
        Log.d(TAG, "3 "+ totalDiff);
        totalDiff += Math.abs(mLeftEarTipX - thisLeftEarTipX);
        Log.d(TAG, "4 "+ totalDiff);
        totalDiff += Math.abs(mLeftEyeX - thisLeftEyeX);
        Log.d(TAG, "5 "+ totalDiff);
        totalDiff += Math.abs(mLeftMouthX - thisLeftMouthX);
        Log.d(TAG, "6 "+ totalDiff);
        totalDiff += Math.abs(mNoseBaseX - thisNoseBaseX);
        Log.d(TAG, "7 "+ totalDiff);
        totalDiff += Math.abs(mRightCheekX - thisRightCheekX);
        Log.d(TAG, "8 "+ totalDiff);
        totalDiff += Math.abs(mRightEarX - thisRightEarX);
        Log.d(TAG, "9 "+ totalDiff);
        totalDiff += Math.abs(mRightEarTipX - thisRightEarTipX);
        Log.d(TAG, "10 "+ totalDiff);
        totalDiff += Math.abs(mRightEyeX - thisRightEyeX);
        Log.d(TAG, "11 "+ totalDiff);
        totalDiff += Math.abs(mRightMouthX - thisRightMouthX);
        Log.d(TAG, "12 "+ totalDiff);
        totalDiff += Math.abs(mBottomMouthY - thisBottomMouthY);
        Log.d(TAG, "13 "+ totalDiff);
        totalDiff += Math.abs(mLeftCheekY - thisLeftCheekY);
        Log.d(TAG, "14 "+ totalDiff);
        totalDiff += Math.abs(mLeftEarY - thisLeftEarY);
        Log.d(TAG, "15 "+ totalDiff);
        totalDiff += Math.abs(mLeftEarTipY - thisLeftEarTipY);
        Log.d(TAG, "16 "+ totalDiff);
        totalDiff += Math.abs(mLeftEyeY - thisLeftEyeY);
        Log.d(TAG, "17 "+ totalDiff);
        totalDiff += Math.abs(mLeftMouthY - thisLeftMouthY);
        Log.d(TAG, "18 "+ totalDiff);
        totalDiff += Math.abs(mNoseBaseY - thisNoseBaseY);
        Log.d(TAG, "19 "+ totalDiff);
        totalDiff += Math.abs(mRightCheekY - thisRightCheekY);
        Log.d(TAG, "20 "+ totalDiff);
        totalDiff += Math.abs(mRightEarY - thisRightEarY);
        Log.d(TAG, "21 "+ totalDiff);
        totalDiff += Math.abs(mRightEarTipY - thisRightEarTipY);
        Log.d(TAG, "22 "+ totalDiff);
        totalDiff += Math.abs(mRightEyeY - thisRightEyeY);
        Log.d(TAG, "23 "+ totalDiff);
        totalDiff += Math.abs(mRightMouthY - thisRightMouthY);
        Log.d(TAG, "24 "+ totalDiff);
        totalDiff += Math.abs(yScale/xScale - mHWRatio);
        Log.d(TAG, "25 "+ totalDiff);
        totalDiff += Math.abs(mEyeDist - Math.pow(thisRightEyeX - thisLeftEyeX, 2) + Math.pow(thisRightEyeY - thisLeftEyeY, 2));
        Log.d(TAG, "26 "+ totalDiff);
        totalDiff += Math.abs(mNoseDist - Math.pow(thisNoseBaseX, 2) + Math.pow(thisNoseBaseY, 2));
        Log.d(TAG, "27 "+ totalDiff);
        totalDiff += Math.abs(mMouthWidth - Math.pow(thisRightMouthX - thisLeftMouthX, 2) + Math.pow(thisRightMouthY - thisLeftMouthY, 2));
        Log.d(TAG, "28 "+ totalDiff);
        totalDiff += Math.abs(mLeftEyeMouthDist - Math.pow(thisLeftEyeX - thisLeftMouthX, 2) + Math.pow(thisLeftEyeY - thisLeftMouthY, 2));
        Log.d(TAG, "29 "+ totalDiff);
        totalDiff += Math.abs(mRightEyeMouthDist - Math.pow(thisRightEyeX - thisRightMouthX, 2) + Math.pow(thisRightEyeY - thisRightMouthY, 2));
        Log.d(TAG, "30 "+ totalDiff);

        return totalDiff;
    }

    public boolean recognize(Face face){
        float totalDiff = comparisonVal(face);
        Log.d("COMPARISON", String.format("%f",totalDiff));
        return totalDiff < THRESHOLD;
    }

    public void store(Face face) {
        resetFaceVals();
        float centerOfFaceX = face.getPosition().x + (face.getWidth() / 2.0f);
        float centerOfFaceY = face.getPosition().y + (face.getHeight() / 2.0f);
        float xScale = face.getWidth();
        float yScale = face.getHeight();
        for(Landmark l : face.getLandmarks()){
            Log.d(TAG, String.format("%d",l.getType()));
            switch(l.getType()){
                case BOTTOM_MOUTH:
                    mBottomMouthX = Math.abs((centerOfFaceX - l.getPosition().x) / xScale);
                    mBottomMouthY = Math.abs((centerOfFaceY - l.getPosition().y) / yScale);
                case LEFT_CHEEK:
                    mLeftCheekX = Math.abs((centerOfFaceX - l.getPosition().x) / xScale);
                    mLeftCheekY = Math.abs((centerOfFaceY - l.getPosition().y) / yScale);
                case LEFT_EAR:
                    mLeftEarX = Math.abs((centerOfFaceX - l.getPosition().x) / xScale);
                    mLeftEarY = Math.abs((centerOfFaceY - l.getPosition().y) / yScale);
                case LEFT_EAR_TIP:
                    mLeftEarTipX = Math.abs((centerOfFaceX - l.getPosition().x) / xScale);
                    mLeftEarTipY = Math.abs((centerOfFaceY - l.getPosition().y) / yScale);
                case LEFT_EYE:
                    mLeftEyeX = Math.abs((centerOfFaceX - l.getPosition().x) / xScale);
                    mLeftEyeY = Math.abs((centerOfFaceY - l.getPosition().y) / yScale);
                case LEFT_MOUTH:
                    mLeftMouthX = Math.abs((centerOfFaceX - l.getPosition().x) / xScale);
                    mLeftMouthY = Math.abs((centerOfFaceY - l.getPosition().y) / yScale);
                case NOSE_BASE:
                    mNoseBaseX = Math.abs((centerOfFaceX - l.getPosition().x) / xScale);
                    mNoseBaseY = Math.abs((centerOfFaceY - l.getPosition().y) / yScale);
                case RIGHT_CHEEK:
                    mRightCheekX = Math.abs((l.getPosition().x - centerOfFaceX) / xScale);
                    mRightCheekY = Math.abs((l.getPosition().y - centerOfFaceY) / yScale);
                case RIGHT_EAR:
                    mRightEarX = Math.abs((l.getPosition().x - centerOfFaceX) / xScale);
                    mRightEarY = Math.abs((l.getPosition().y - centerOfFaceY) / yScale);
                case RIGHT_EAR_TIP:
                    mRightEarTipX = Math.abs((l.getPosition().x - centerOfFaceX) / xScale);
                    mRightEarTipY = Math.abs((l.getPosition().y - centerOfFaceY) / yScale);
                case RIGHT_EYE:
                    mRightEyeX = Math.abs((l.getPosition().x - centerOfFaceX) / xScale);
                    mRightEyeY = Math.abs((l.getPosition().y - centerOfFaceY) / yScale);
                case RIGHT_MOUTH:
                    mRightMouthX = Math.abs((l.getPosition().x - centerOfFaceX) / xScale);
                    mRightMouthY = Math.abs((l.getPosition().y - centerOfFaceY) / yScale);
            }

            mHWRatio = yScale/xScale;
            mEyeDist = Math.pow(mRightEyeX - mLeftEyeX, 2) + Math.pow(mRightEyeY - mLeftEyeY, 2);
            mNoseDist = Math.pow(mNoseBaseX, 2) + Math.pow(mNoseBaseY, 2);
            mMouthWidth = Math.pow(mRightMouthX - mLeftMouthX, 2) + Math.pow(mRightMouthY - mLeftMouthY, 2);
            mLeftEyeMouthDist = Math.pow(mLeftEyeX - mLeftMouthX, 2) + Math.pow(mLeftEyeY - mLeftMouthY, 2);
            mRightEyeMouthDist = Math.pow(mRightEyeX - mRightMouthX, 2) + Math.pow(mRightEyeY - mRightMouthY, 2);
        }

        Log.d(TAG, "Stored facial coordinates");
    }

    private void resetFaceVals(){
        mBottomMouthX = 0;
        mLeftCheekX = 0;
        mLeftEarX = 0;
        mLeftEarTipX = 0;
        mLeftEyeX = 0;
        mLeftMouthX = 0;
        mNoseBaseX = 0;
        mRightCheekX = 0;
        mRightEarX = 0;
        mRightEarTipX = 0;
        mRightEyeX = 0;
        mRightMouthX = 0;
        mBottomMouthY = 0;
        mLeftCheekY = 0;
        mLeftEarY = 0;
        mLeftEarTipY = 0;
        mLeftEyeY = 0;
        mLeftMouthY = 0;
        mNoseBaseY = 0;
        mRightCheekY = 0;
        mRightEarY = 0;
        mRightEarTipY = 0;
        mRightEyeY = 0;
        mRightMouthY = 0;
    }
}

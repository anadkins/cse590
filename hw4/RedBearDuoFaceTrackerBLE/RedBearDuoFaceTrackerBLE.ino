#include "ble_config.h"

#if defined(ARDUINO) 
SYSTEM_MODE(SEMI_AUTOMATIC); 
#endif

// Must be an integer between 1 and 9 and and must also be set to len(BLE_SHORT_NAME) + 1
#define BLE_SHORT_NAME_LEN 9
#define BLE_SHORT_NAME 'A','N','A','D','K','I','N','S'  
#define RECEIVE_MAX_LEN 3
#define SEND_MAX_LEN 3
#define MIN_SERVO_ANGLE  55
#define MAX_SERVO_ANGLE  115
#define c 261    // 261 Hz
#define d 294    // 294 Hz
#define e 329    // 329 Hz
#define f 349    // 349 Hz
#define gf 370    // 392 Hz
#define g 392    // 392 Hz
#define a 440    // 440 Hz
#define bf 466    // 493 Hz
#define b 493    // 493 Hz
#define C 523    // 523 Hz

// Define the pins on the Duo boardrec
#define BUZZER_PIN D0
#define BLE_DEVICE_CONNECTED_DIGITAL_OUT_PIN D1
#define SERVO_PIN D2
#define ULTRASONIC_PIN D3
#define SEND_DATA_PIN D4

#define ECHO_PIN D8
#define TRIG_PIN D9

// Define variables for ultrasonic smoothing
const int SMOOTHING_WINDOW_SIZE = 10;
int _readings[SMOOTHING_WINDOW_SIZE];      // the readings from the analog input
int _readIndex = 0;              // the index of the current reading
int _total = 0;                  // the running total
int _average = 0;                // the average

// store BLE connection status
bool _connected = false;
bool _sending = false;

// Anything over 400 cm (23200 us pulse) is "out of range"
const unsigned int MAX_DIST = 23200;

Servo _posServo;

// Device connected and disconnected callbacks
void deviceConnectedCallback(BLEStatus_t status, uint16_t handle);
void deviceDisconnectedCallback(uint16_t handle);

// UUID is used to find the device by other BLE-abled devices
static uint8_t service1_uuid[16]    = { 0x71,0x3d,0x00,0x00,0x50,0x3e,0x4c,0x75,0xba,0x94,0x31,0x48,0xf1,0x8d,0x94,0x1e };
static uint8_t service1_tx_uuid[16] = { 0x71,0x3d,0x00,0x03,0x50,0x3e,0x4c,0x75,0xba,0x94,0x31,0x48,0xf1,0x8d,0x94,0x1e };
static uint8_t service1_rx_uuid[16] = { 0x71,0x3d,0x00,0x02,0x50,0x3e,0x4c,0x75,0xba,0x94,0x31,0x48,0xf1,0x8d,0x94,0x1e };

// Define the receive and send handlers
static uint16_t receive_handle = 0x0000; // recieve
static uint16_t send_handle = 0x0000; // send

static uint8_t receive_data[RECEIVE_MAX_LEN] = { 0x01 };
int bleReceiveDataCallback(uint16_t value_handle, uint8_t *buffer, uint16_t size); // function declaration for receiving data callback
static uint8_t send_data[SEND_MAX_LEN] = { 0x00 };

// Define the configuration data
static uint8_t adv_data[] = {
  0x02,
  BLE_GAP_AD_TYPE_FLAGS,
  BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE, 
  
  BLE_SHORT_NAME_LEN,
  BLE_GAP_AD_TYPE_SHORT_LOCAL_NAME,
  BLE_SHORT_NAME, 
  
  0x11,
  BLE_GAP_AD_TYPE_128BIT_SERVICE_UUID_COMPLETE,
  0x1e,0x94,0x8d,0xf1,0x48,0x31,0x94,0xba,0x75,0x4c,0x3e,0x50,0x00,0x00,0x3d,0x71 
};

static btstack_timer_source_t send_characteristic;
static void bleSendDataTimerCallback(btstack_timer_source_t *ts); // function declaration for sending data callback
int _sendDataFrequency = 200; // 200ms (how often to read the pins and transmit the data to Android)
bool recognized = false;

void setup() {
  Serial.begin(115200);
  delay(5000);
  Serial.println("Face Tracker BLE");

  for (int i = 0; i < SMOOTHING_WINDOW_SIZE; i++) {
    _readings[i] = 0;
  }
  
  // Initialize ble_stack.
  ble.init();
  
  // Register BLE callback functions
  ble.onConnectedCallback(bleConnectedCallback);
  ble.onDisconnectedCallback(bleDisconnectedCallback);

  //lots of standard initialization hidden in here - see ble_config.cpp
  configureBLE(); 
  
  // Set BLE advertising data
  ble.setAdvertisementData(sizeof(adv_data), adv_data);
  
  // Register BLE callback functions
  ble.onDataWriteCallback(bleReceiveDataCallback);

  // Add user defined service and characteristics
  ble.addService(service1_uuid);
  receive_handle = ble.addCharacteristicDynamic(service1_tx_uuid, ATT_PROPERTY_NOTIFY|ATT_PROPERTY_WRITE|ATT_PROPERTY_WRITE_WITHOUT_RESPONSE, receive_data, RECEIVE_MAX_LEN);
  send_handle = ble.addCharacteristicDynamic(service1_rx_uuid, ATT_PROPERTY_NOTIFY, send_data, SEND_MAX_LEN);

  // BLE peripheral starts advertising now.
  ble.startAdvertising();
  Serial.println("BLE start advertising.");

  // Setup pins
  pinMode(BLE_DEVICE_CONNECTED_DIGITAL_OUT_PIN, OUTPUT);
  pinMode(ULTRASONIC_PIN, OUTPUT);
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  pinMode(SEND_DATA_PIN, OUTPUT);
  digitalWrite(SEND_DATA_PIN, LOW);
  digitalWrite(TRIG_PIN, LOW);
  _posServo.attach(SERVO_PIN);
  _posServo.write( (int)((MAX_SERVO_ANGLE - MIN_SERVO_ANGLE) / 2.0) );

  // Start a task to check status of the pins on your RedBear Duo
  // Works by polling every X milliseconds where X is _sendDataFrequency
  send_characteristic.process = &bleSendDataTimerCallback;
  ble.setTimer(&send_characteristic, _sendDataFrequency); 
  ble.addTimer(&send_characteristic);
}

void loop() 
{
  // Read from the ultrasonic sensor
  unsigned long t1;
  unsigned long t2;
  unsigned long pulse_width;
  float cm;
  float inches;

  // Hold the trigger pin high for at least 10 us
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);

  // Wait for pulse on echo pin
  while ( digitalRead(ECHO_PIN) == 0 );
  
  // Measure how long the echo pin was held high (pulse width)
  // Note: the micros() counter will overflow after ~70 min
  t1 = micros();
  while ( digitalRead(ECHO_PIN) == 1);
  t2 = micros();
  pulse_width = t2 - t1;

  // Calculate distance in centimeters and inches. The constants are found in the datasheet,
  // and calculated from the assumed speed of sound in air at sea level (~340 m/s).
  // Datasheet: https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf
  cm = pulse_width / 58.0;
  inches = pulse_width / 148.0;
  
  // subtract the last reading
  _total = _total - _readings[_readIndex];
  
  // read from the sensor
  int curReading = cm;
  _readings[_readIndex] = curReading;
  
  // add the reading to the total
  _total = _total + _readings[_readIndex];
  
  // advance to the next position in the array
  _readIndex = _readIndex + 1;

  // if we're at the end of the array...
  if (_readIndex >= SMOOTHING_WINDOW_SIZE) {
    // ...wrap around to the beginning:
    _readIndex = 0;
  }

  // calculate the average
  _average = _total / SMOOTHING_WINDOW_SIZE;
  
  // Print out results
  if ( pulse_width > MAX_DIST ) {
    Serial.println("Out of range");
  } else {
    Serial.print(cm);
    Serial.print(" cm \t");
    Serial.print(inches);
    Serial.println(" in");
  }

  if(_average < 50 && !recognized){
    digitalWrite(ULTRASONIC_PIN, HIGH);
    tone(BUZZER_PIN, b, 100);
    delay(100);
    digitalWrite(ULTRASONIC_PIN, LOW);
    tone(BUZZER_PIN, a, 100);
  }
  else{
    digitalWrite(ULTRASONIC_PIN, LOW);
    noTone(BUZZER_PIN);
  }

  // Send the results in cm to the android
  
  
  // The HC-SR04 datasheet recommends waiting at least 60ms before next measurement
  // in order to prevent accidentally noise between trigger and echo
  // See: https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf
  delay(60);
}

/**
 * @brief Connect handle.
 *
 * @param[in]  status   BLE_STATUS_CONNECTION_ERROR or BLE_STATUS_OK.
 * @param[in]  handle   Connect handle.
 *
 * @retval None
 */
void bleConnectedCallback(BLEStatus_t status, uint16_t handle) {
  switch (status) {
    case BLE_STATUS_OK:
      Serial.println("BLE device connected!");
      _connected = true;
      digitalWrite(BLE_DEVICE_CONNECTED_DIGITAL_OUT_PIN, HIGH);
      break;
    default: break;
  }
}

/**
 * @brief Disconnect handle.
 *
 * @param[in]  handle   Connect handle.
 *
 * @retval None
 */
void bleDisconnectedCallback(uint16_t handle) {
  Serial.println("BLE device disconnected.");
  _connected = false;
  digitalWrite(BLE_DEVICE_CONNECTED_DIGITAL_OUT_PIN, LOW);
}

/**
 * @brief Callback for receiving data from Android (or whatever device you're connected to).
 *
 * @param[in]  value_handle  
 * @param[in]  *buffer       The buffer pointer of writting data.
 * @param[in]  size          The length of writting data.   
 *
 * @retval 
 */
int bleReceiveDataCallback(uint16_t value_handle, uint8_t *buffer, uint16_t size) {

  if (receive_handle == value_handle) {
    memcpy(receive_data, buffer, RECEIVE_MAX_LEN);
    Serial.print("Received data: ");
    for (uint8_t index = 0; index < RECEIVE_MAX_LEN; index++) {
      Serial.print(receive_data[index]);
      Serial.print(" ");
    }
    Serial.println(" ");
    
    // process the data. 
    if (receive_data[0] == 0x01) { //receive the face data
      _posServo.write(map(receive_data[1], 0, 255, MIN_SERVO_ANGLE, MAX_SERVO_ANGLE));
      if(receive_data[2] == 0x00){
        recognized = false;
      }
      else{
        recognized = true;
      }
    }
  }
  return 0;
}

/**
 * @brief Timer task for sending status change to client.
 * @param[in]  *ts   
 * @retval None
 * 
 * Send the data from either analog read or digital read back to 
 * the connected BLE device (e.g., Android)
 */
static void bleSendDataTimerCallback(btstack_timer_source_t *ts) {
  Serial.println("Send distance data via BLE");
  // send the ultrasonic data
  if(_connected){
    send_data[0] = (0x0B);
    send_data[1] = (_average >> 8);
    send_data[2] = (_average);
    if (ble.attServerCanSendPacket()){
      _sending = !_sending;
      if(_sending){
        digitalWrite(SEND_DATA_PIN, HIGH);
      }
      else{
        digitalWrite(SEND_DATA_PIN, LOW);
      }
      Serial.print("Sending ");
      Serial.println(_average);
      ble.sendNotify(send_handle, send_data, SEND_MAX_LEN);
    }
  }
  else{
    digitalWrite(SEND_DATA_PIN, LOW);
  }
  // Restart timer.
  ble.setTimer(ts, _sendDataFrequency);
  ble.addTimer(ts);
}

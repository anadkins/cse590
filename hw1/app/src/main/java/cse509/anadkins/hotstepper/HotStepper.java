package cse509.anadkins.hotstepper;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Arrays;
import java.util.Random;

/*
 * HotStepper is the main activity and sensor listener which detects accelerometer events
 * and updates the user interface based on calculations done by the StepSensor class
 */
public class HotStepper extends AppCompatActivity implements SensorEventListener {

    final int GRAPH_SIZE = 480;
    final int STEP_GOAL = 200;
    final static long DEBUG_DELAY = 100000000;
    int _color = 0;
    int _count = 0;
    int _detectorSteps = 0;
    int _x = 1;
    long _lastUpdate = 0;
    float _androidStepOffset = -1;
    float _androidSteps = 0;
    boolean _showDebug = false;
    LineGraphSeries<DataPoint> _series;
    LineGraphSeries<DataPoint> _rawSeries;
    Random _rnd = new Random();
    Sensor _acc;
    Sensor _androidStepSensor;
    Sensor _androidStepDetector;
    SensorManager _sm;
    StepSensor _ss;

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hot_stepper);
        init();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // if accelerometer event, send info to StepSensor
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            _ss.update(event.timestamp, event.values);
            float sum = _ss.currentSum();
            float rawSum = _ss.sum(event.values);
            updateGraph(sum, rawSum);
            if(_showDebug && event.timestamp - _lastUpdate > DEBUG_DELAY){
                // set some debug info text
                _lastUpdate = event.timestamp;
                TextView debug = this.findViewById(R.id.debugText);
                debug.setText(Arrays.toString(event.values) + "\nRaw Sum: " + rawSum + "\nFiltered: " + sum);
            }
        }
        else if(event.sensor.getType() == Sensor.TYPE_STEP_COUNTER){
            TextView stepCounter = this.findViewById(R.id.stepCounter);
            _androidSteps = event.values[0];
            if(_androidStepOffset == -1){ // initialization step
                _androidStepOffset = _androidSteps;
            }
            stepCounter.setText("STEP_COUNTER says " + String.format(java.util.Locale.US, "%.0f", _androidSteps - _androidStepOffset) + " steps");
        }
        else if(event.sensor.getType() == Sensor.TYPE_STEP_DETECTOR){
            _detectorSteps++;
            TextView detectorCounter = this.findViewById(R.id.stepDetector);
            detectorCounter.setText("STEP_DETECTOR says " + _detectorSteps + " steps");
        }
    }

    public void step(){
        // StepSensor calls this method to report a step being taken
        _count++;
        TextView stepView = this.findViewById(R.id.stepView);
        stepView.setText(_count + " steps");
        // fun thing, change button colors randomly every step
        //int newColor = Color.argb(255, _rnd.nextInt(256), _rnd.nextInt(256), _rnd.nextInt(256));
        // new fun thing, increase color slowly
        int color = newColor();
        Button resetButton = this.findViewById(R.id.resetButton);
        resetButton.setBackgroundColor(color);
        Button debugButton = this.findViewById(R.id.debugButton);
        debugButton.setBackgroundColor(color);
    }

    private void init() {
        initButton();
        initGraph();
        initSensor();
    }

    private void initButton(){
        // set up the reset button which resets step counters to zero
        Button resetButton = this.findViewById(R.id.resetButton);
        resetButton.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v){
                _count = 0;
                _x = 0;
                _color = 0;
                _androidStepOffset = _androidSteps;
                _rawSeries.resetData(new DataPoint[]{});
                _series.resetData(new DataPoint[]{});

                TextView stepView = ((View) v.getParent()).findViewById(R.id.stepView);
                stepView.setText("0 steps");
                TextView stepCounter = ((View) v.getParent()).findViewById(R.id.stepCounter);
                stepCounter.setText("STEP_COUNTER says 0 steps");
                _detectorSteps = 0;
                TextView stepDetector = ((View) v.getParent()).findViewById(R.id.stepDetector);
                stepDetector.setText("STEP_DETECTOR says 0 steps");
            }
        });
        // set up the debug button which shows the raw data graph and text
        Button debugButton = this.findViewById(R.id.debugButton);
        debugButton.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v){
                _showDebug = ! _showDebug;
                if(!_showDebug){
                    TextView debugView = ((View) v.getParent()).findViewById(R.id.debugText);
                    debugView.setText("");
                    GraphView rawGraph = (GraphView) findViewById(R.id.sensorGraphRaw);
                    rawGraph.setVisibility(View.INVISIBLE);
                }
                else{
                    GraphView rawGraph = (GraphView) findViewById(R.id.sensorGraphRaw);
                    rawGraph.setVisibility(View.VISIBLE);
                    // make sure the text is visible
                    TextView debugView = ((View) v.getParent()).findViewById(R.id.debugText);
                    debugView.bringToFront();
                }
            }
        });
    }

    private int newColor(){
        // idea for color cycling adapted from
        // https://krazydad.com/tutorials/makecolors.php
        double freq = 2 * Math.PI / STEP_GOAL;
        int center = 128;
        int width = 127;
        _color++;
        int red = (int) (Math.sin(freq*_color + 0) * width + center);
        int green = (int) (Math.sin(freq*_color + 2) * width + center);
        int blue = (int) (Math.sin(freq*_color + 4) * width + center);
        return Color.argb(115, red, green, blue);
    }

    private void initGraph(){
        GraphView graph = (GraphView) findViewById(R.id.sensorGraph);
        _series = new LineGraphSeries<>();
        graph.addSeries(_series);
        GraphView rawGraph = (GraphView) findViewById(R.id.sensorGraphRaw);
        _rawSeries = new LineGraphSeries<>();
        rawGraph.addSeries(_rawSeries);
        rawGraph.setVisibility(View.INVISIBLE);
    }

    private void initSensor() {
        _sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        _ss = new StepSensor(this);
        if(_sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
            _acc = _sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            _sm.registerListener(this, _acc, SensorManager.SENSOR_DELAY_FASTEST);
        }
        if(_sm.getDefaultSensor(Sensor.TYPE_STEP_COUNTER) != null){
            _androidStepSensor = _sm.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
            _sm.registerListener(this, _androidStepSensor, Sensor.REPORTING_MODE_ON_CHANGE);
        }
        if(_sm.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null){
            _androidStepDetector = _sm.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
            _sm.registerListener(this, _androidStepDetector, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    private void updateGraph(float val, float rawVal){
        // add the new values to the graphs
        _series.appendData(new DataPoint(_x, val), false, GRAPH_SIZE);
        _rawSeries.appendData(new DataPoint(_x, rawVal), false, GRAPH_SIZE);
        _x++;
    }
}

package cse509.anadkins.hotstepper;

/*
 * StepSensor manages step detection based on the sensor event data provided by the activity
 * and reports when a step is detected to the listening HotStepper activity
 */
public class StepSensor {

    final static int SMOOTHING_WINDOW_SIZE = 125;
    final static float STEP_THRESHOLD = 70f;
    final static long STEP_DELAY = 400000000;

    private HotStepper _listener;
    private float _accelValueHistory[][] = new float[SMOOTHING_WINDOW_SIZE][3];
    private float _curAccelAvg[] = new float[3];
    private float _gravity[] = new float[3];
    private float _processedAcceleration[] = new float[3];
    private float _linearAcceleration[] = new float[3];
    private float _rawAccelerometerValues[] = new float[3];
    private long _lastAccelChangeNs = -1;
    private float _oldVelo = Float.MAX_VALUE;
    private long _lastStep = 0;

    public StepSensor(HotStepper hotStepper) {
        _listener = hotStepper;
    }

    public float sum(float[] array) {
        float val = 0;
        for (int i = 0; i < array.length; i++) {
            val += array[i];
        }
        return val;
    }

    public float currentSum(){
        return sum(_curAccelAvg);
    }

    public float[] currentAvg(){
        return _curAccelAvg;
    }

    public void update(long timestamp, float[] values){
        if(_lastAccelChangeNs == -1){
            _lastAccelChangeNs = timestamp;
        }
        if(timestamp - _lastAccelChangeNs < STEP_DELAY / SMOOTHING_WINDOW_SIZE){
            // not enough time has passed since last update
            return;
        }

        _rawAccelerometerValues[0] = values[0];
        _rawAccelerometerValues[1] = values[1];
        _rawAccelerometerValues[2] = values[2];

        //smooth the accelerometer signal and remove gravity
        final float alpha = 0.7f;

        //Isolate the force of gravity with the low-pass filter.
        _gravity[0] = alpha * _gravity[0] + (1 - alpha) * _rawAccelerometerValues[0];
        _gravity[1] = alpha * _gravity[1] + (1 - alpha) * _rawAccelerometerValues[1];
        _gravity[2] = alpha * _gravity[2] + (1 - alpha) * _rawAccelerometerValues[2];

        //Remove the gravity contribution with the high-pass filter.
        _linearAcceleration[0] = _rawAccelerometerValues[0] - _gravity[0];
        _linearAcceleration[1] = _rawAccelerometerValues[1] - _gravity[1];
        _linearAcceleration[2] = _rawAccelerometerValues[2] - _gravity[2];

        //Processed the acceleration as an absolute value
        final int multiplier = 50;
        _processedAcceleration[0] = Math.abs(_linearAcceleration[0]) * multiplier;
        _processedAcceleration[1] = Math.abs(_linearAcceleration[1]) * multiplier;
        _processedAcceleration[2] = Math.abs(_linearAcceleration[2]) * multiplier;

        updateVeloHistory();
        updateAccelAvg();

        float veloEstimate = currentSum();
        long timePassed = timestamp - _lastStep;

        // Check for a step
        if (veloEstimate > STEP_THRESHOLD &&_oldVelo <= STEP_THRESHOLD && (timePassed > STEP_DELAY)){
            _listener.step();
            _lastStep = timestamp;
        }
        _oldVelo = veloEstimate;
        _lastAccelChangeNs = timestamp;
    }

    private void updateVeloHistory(){
        // updates our historical values used to smooth the velocity
        float[] next = _processedAcceleration;
        for(int i = 0; i < SMOOTHING_WINDOW_SIZE; i++){
            float[] tmp = _accelValueHistory[i];
            _accelValueHistory[i] = next;
            next = tmp;
        }
    }

    private void updateAccelAvg(){
        // find the average from our historical values
        float[] totals = {0f, 0f, 0f};
        for(int i = 0; i < SMOOTHING_WINDOW_SIZE; i++){
            for(int j = 0; j < _accelValueHistory[i].length; j++){
                totals[j] += _accelValueHistory[i][j];
            }
        }

        for(int i = 0; i < totals.length; i++){
            _curAccelAvg[i] = totals[i] / SMOOTHING_WINDOW_SIZE;
        }
    }
}
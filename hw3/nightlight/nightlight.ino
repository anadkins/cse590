#include "ble_config.h"

#if defined(ARDUINO) 
SYSTEM_MODE(SEMI_AUTOMATIC); 
#endif

#define RECEIVE_MAX_LEN 4
#define BLE_SHORT_NAME_LEN 0x09
#define BLE_SHORT_NAME 'A','N','A','D','K','I','N','S'
#define COMMON_ANODE 1
#define c 261    // 261 Hz
#define d 294    // 294 Hz
#define e 329    // 329 Hz
#define f 349    // 349 Hz
#define gf 370    // 392 Hz
#define g 392    // 392 Hz
#define a 440    // 440 Hz
#define bf 466    // 493 Hz
#define b 493    // 493 Hz
#define C 523    // 523 Hz

const int CENTER = 128;
const int WIDTH = 127;
const double FREQ = 2 * 3.14159 / 4100;
const int RED_LEDS_4 = D5;
const int RED_LEDS_3 = D4;
const int RED_LEDS_2  = D3;
const int RED_LEDS_1  = D2;
const int BUZZER_PIN = D0;
const int RGB_RED_PIN = D17;
const int RGB_GREEN_PIN  = D16;
const int RGB_BLUE_PIN  = D15;
const int PHOTOSENSOR_INPUT = A0;
const int POT_INPUT = A1;
const int MOTOR_OUTPUT = D1;
const int DELAY = 200; // delay between changing colors
Servo myservo;  // create servo
bool phoneColor = false;
static uint8_t service1_uuid[16]    = { 0x71,0x3d,0x00,0x00,0x50,0x3e,0x4c,0x75,0xba,0x94,0x31,0x48,0xf1,0x8d,0x94,0x1e };
static uint8_t service1_tx_uuid[16] = { 0x71,0x3d,0x00,0x03,0x50,0x3e,0x4c,0x75,0xba,0x94,0x31,0x48,0xf1,0x8d,0x94,0x1e };

static uint8_t adv_data[] = {
  0x02,
  BLE_GAP_AD_TYPE_FLAGS,
  BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE, 
  
  BLE_SHORT_NAME_LEN,
  BLE_GAP_AD_TYPE_SHORT_LOCAL_NAME,
  BLE_SHORT_NAME, 
  
  0x11,
  BLE_GAP_AD_TYPE_128BIT_SERVICE_UUID_COMPLETE,
  0x1e,0x94,0x8d,0xf1,0x48,0x31,0x94,0xba,0x75,0x4c,0x3e,0x50,0x00,0x00,0x3d,0x71 
};


static uint16_t receive_handle = 0x0000; // recieve
static uint8_t receive_data[RECEIVE_MAX_LEN] = { 0x01 };

void setup() {
  // Turn on Serial so we can verify expected colors via Serial Monitor
  Serial.begin(9600); 

  pinMode(RGB_RED_PIN, OUTPUT);
  pinMode(RGB_GREEN_PIN, OUTPUT);
  pinMode(RGB_BLUE_PIN, OUTPUT);
  pinMode(RED_LEDS_1, OUTPUT);
  pinMode(RED_LEDS_2, OUTPUT);
  pinMode(RED_LEDS_3, OUTPUT);
  pinMode(RED_LEDS_4, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  pinMode(POT_INPUT, INPUT);
  pinMode(PHOTOSENSOR_INPUT, INPUT);
  myservo.attach(MOTOR_OUTPUT);
  myservo.write(0);
  
  // Initialize ble_stack.
  ble.init();
  configureBLE(); //lots of standard initialization hidden in here - see ble_config.cpp
  ble.onDisconnectedCallback(disablePhone);
  // Set BLE advertising data
  ble.setAdvertisementData(sizeof(adv_data), adv_data);

  // Register BLE callback functions
  ble.onDataWriteCallback(bleWriteCallback);

  // Add user defined service and characteristics
  ble.addService(service1_uuid);
  receive_handle = ble.addCharacteristicDynamic(service1_tx_uuid, ATT_PROPERTY_NOTIFY|ATT_PROPERTY_WRITE|ATT_PROPERTY_WRITE_WITHOUT_RESPONSE, receive_data, RECEIVE_MAX_LEN);
  
  // BLE peripheral starts advertising now.
  ble.startAdvertising();
  Serial.println("BLE start advertising.");

  // turn off red LEDs
  digitalWrite(RED_LEDS_1, HIGH);
  digitalWrite(RED_LEDS_2, HIGH);
  digitalWrite(RED_LEDS_3, HIGH);
  digitalWrite(RED_LEDS_4, HIGH);
}

void disablePhone(uint16_t handle) {
  Serial.println("Disconnected.");
  phoneColor = false;
}

void loop() {
  // GET INPUTS
  int potValue = analogRead(POT_INPUT);
  int lightValue = map(pow(analogRead(PHOTOSENSOR_INPUT), 2), 0, 16000000, 1, 20);
  
  // TAKE ACTIONS
  if(!phoneColor){
    setVariableColor(potValue, lightValue);
  }
  if(lightValue < 2){
    initiateScaryTimes();
  }else{
    myservo.write(0);
  }
}

/**
 * @brief Callback for writing event.
 *
 * @param[in]  value_handle  
 * @param[in]  *buffer       The buffer pointer of writting data.
 * @param[in]  size          The length of writting data.   
 *
 * @retval 
 */
int bleWriteCallback(uint16_t value_handle, uint8_t *buffer, uint16_t size) {
  Serial.print("Write value handler: ");
  Serial.println(value_handle, HEX);

  if (receive_handle == value_handle) {
    memcpy(receive_data, buffer, RECEIVE_MAX_LEN);
    Serial.print("Write value: ");
    for (uint8_t index = 0; index < RECEIVE_MAX_LEN; index++) {
      Serial.print(receive_data[index], HEX);
      Serial.print(" ");
    }
    Serial.println(" ");
    
    // Process the data
    if (receive_data[0] == 0x01) { // Command is to control color
      Serial.print("Got data ");
      Serial.print(receive_data[1]);
      Serial.print(receive_data[2]);
      Serial.println(receive_data[3]);
      int red = receive_data[1] & 0xff;
      int green = receive_data[2] & 0xff;
      int blue = receive_data[3] & 0xff;
      Serial.print(red);
      Serial.print(", ");
      Serial.print(green);
      Serial.print(", ");
      Serial.println(blue);
      phoneColor = true;
      setColor(red, green, blue);
    }
    else if (receive_data[0] == 0x02) { // Command is to release control
      Serial.println("got some other other data");
      phoneColor = false;
    }
  }
  return 0;
}

// This is where the fun happens
void initiateScaryTimes(){
  Serial.println("WE ARE GETTING SPOOKY");
  // Turn the motor
  myservo.write(179);
  tone(BUZZER_PIN, a);
  digitalWrite(RED_LEDS_1, LOW);
  digitalWrite(RED_LEDS_2, HIGH);
  digitalWrite(RED_LEDS_3, HIGH);
  digitalWrite(RED_LEDS_4, HIGH);
  delay(DELAY);
  tone(BUZZER_PIN, bf);
  digitalWrite(RED_LEDS_1, HIGH);
  digitalWrite(RED_LEDS_2, LOW);
  digitalWrite(RED_LEDS_3, HIGH);
  digitalWrite(RED_LEDS_4, HIGH);
  delay(DELAY);
  tone(BUZZER_PIN, a);
  digitalWrite(RED_LEDS_1, HIGH);
  digitalWrite(RED_LEDS_2, HIGH);
  digitalWrite(RED_LEDS_3, LOW);
  digitalWrite(RED_LEDS_4, HIGH);
  delay(DELAY);
  tone(BUZZER_PIN, gf);
  digitalWrite(RED_LEDS_1, HIGH);
  digitalWrite(RED_LEDS_2, HIGH);
  digitalWrite(RED_LEDS_3, HIGH);
  digitalWrite(RED_LEDS_4, LOW);
  delay(DELAY);
  noTone(BUZZER_PIN);
  digitalWrite(RED_LEDS_1, HIGH);
  digitalWrite(RED_LEDS_2, HIGH);
  digitalWrite(RED_LEDS_3, HIGH);
  digitalWrite(RED_LEDS_4, HIGH);
}

// set the RGB color based on the potentiometer and ambient light
void setVariableColor(int potValue, int lightValue){
  int red = 2*(int) (sin(FREQ*potValue + 0) * WIDTH + CENTER)/lightValue;
  int green = 2*(int) (sin(FREQ*potValue + 2) * WIDTH + CENTER)/lightValue;
  int blue = 2*(int) (sin(FREQ*potValue + 4) * WIDTH + CENTER)/lightValue;
  setColor(red, green, blue);  // set color
}

void setColor(int red, int green, int blue)
{
  #ifdef COMMON_ANODE
    red = 255 - red;
    green = 255 - green;
    blue = 255 - blue;
  #endif
  analogWrite(RGB_RED_PIN, red);
  analogWrite(RGB_GREEN_PIN, green);
  analogWrite(RGB_BLUE_PIN, blue);
}
